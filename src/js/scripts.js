(function ($) {
  $.fn.slider = function(){

    var slider = $(this);
    var slides = [];
    $(this).append("<div class='slider-indicator'></div>");

    $(this).find('div').each(function(e){
    	e++;
      if(($(this).attr('class') !== "slider-indicator")) {
    	  slides.push(e);
    	  $(".slider-indicator").append("<span id='"+e+"'></span>");
    	  if(e > 1) {
    	    $(this).css({"position":"absolute","top":"0","left":"0"});
    	    $(this).hide();
    	  }
    	}
    });

    $("span#1").addClass('active');
    var length = slides.length;
    var count = 2;

    $(".slider-indicator span").click(function(){
    	clearInterval(autoSlide);
      count = $(this).attr('id');
      slider.find('div').each(function(e){
        if($(this).attr('class') !== "slider-indicator") {
          e++;
          if(e == count) {
            $(this).fadeIn();
            $(this).css({"position":"relative","top":"0","left":"0"});
            $("span#"+e).addClass('active');
            $("span#"+e).siblings().removeClass('active');              
          } else {
            $(this).fadeOut();
            $(this).css({"position":"absolute","top":"0","left":"0"});
          }
        }
      });
    });              


    function autoSlide() {        

      slider.find('div').each(function(e){
        if($(this).attr('class') !== "slider-indicator") {
          e++;
          if(e == count) {
            $(this).fadeIn();
            $(this).css({"position":"relative","top":"0","left":"0"});
            $("span#"+e).addClass('active');
            $("span#"+e).siblings().removeClass('active');
          } else {
            $(this).fadeOut();
            $(this).css({"position":"absolute","top":"0","left":"0"});
          }
        }
      });

      count++;
      if(count == parseInt(length) + 1) {
        count = 1;
      }           
    }

    setInterval(autoSlide, 8000);       
  return this;
    };

}(jQuery));

$(function(){
  $('.slider').slider();
});